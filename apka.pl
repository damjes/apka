% SWI-Prolog

:- [library(http/thread_httpd), library(http/http_dispatch), library(http/html_write)].

% stworz mutex
:- mutex_create(_, [alias(licznik)]).

:- dynamic odwiedzin/1.

odwiedzin(0).

inkrementujLicznik(X1) :-
	odwiedzin(X), !,
	retractall(odwiedzin(_)),
	X1 is X+1,
	asserta(odwiedzin(X1)).

pokazLicznik(X) :-
	with_mutex(licznik, inkrementujLicznik(X)).

:- http_handler('/', hello, []).
:- http_handler('/licznik', licznik, []).
:- http_handler('/prolog/licznik', licznik, []).

server(Port) :-
        http_server(http_dispatch, [port(Port)]).

hello(_Request) :-
	reply_html_page(title('Hello World'), [
		h1('Hello World'),
		p(['This example demonstrates generating HTML ',
			'messages from Prolog']),
		a(href('prolog/licznik'), 'Licznik')
		]).

licznik(_Request) :-
	pokazLicznik(X),
	reply_html_page(title('Hello World'), [
		h1('Licznik'),
		p(['Jestes ',
			X,
			' osoba na stronie'])]).

